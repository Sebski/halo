<div class="container-hero hero-background d-flex justify-content-center justify-content-lg-start" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 4.png'); background-position: -30em 0em;">
    <div class="row position-relative d-flex">
        <div class="col-lg-6 col-md-6 col-sm-12 test">
            <div>
                <h2 class="hero-title mobile-hero-title">Nasze <br><span class="hero-accent-color">pokoje</span>
                </h2>
            </div>
            <div class="halo-bg-btn d-flex">
                <a class="halo-btn" href="/oferta/">
                    SPRAWDŹ
                </a>
            </div>
        </div>
    </div>
</div>
