<?php

add_post_type_support( 'osoby', 'excerpt' );
add_post_type_support( 'news', 'excerpt' );
add_post_type_support( 'new', 'excerpt' );
add_post_type_support( 'aktualnosci', 'excerpt' );
add_post_type_support( 'publication', 'excerpt' );
add_post_type_support( 'publikacje', 'excerpt' );
add_post_type_support( 'publications', 'excerpt' );

function render($template, array $data)
{
  extract($data);
  ob_start();
  include($template);
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}

 function getOther($foo){
    $args = array(
        'post__not_in' => array($foo),
        'post_type' => 'news',
        'post_status' => 'publish',
        'posts_per_page' => 2,
        'orderby' => 'date',
        'order' => 'ASC',
    );
    return new WP_Query( $args );
}

add_action('rest_api_init', function () {
  register_rest_route('paginacja/v1', '/pl/getNews/(?P<strona>[^.]+)', array(
    'methods' => 'GET',
    'callback' => 'getNewsPl',
  ));
});
add_action('rest_api_init', function () {
    register_rest_route('paginacja/v1', '/en/getNews/(?P<strona>[^.]+)', array(
        'methods' => 'GET',
        'callback' => 'getNewsEn',
    ));
});


add_action('rest_api_init', function () {
  register_rest_route('paginacja/v1', '/pl/getPublications/(?P<strona>[^.]+)', array(
    'methods' => 'GET',
    'callback' => 'getPublicationsPl',
  ));
});
add_action('rest_api_init', function () {
    register_rest_route('paginacja/v1', '/en/getPublications/(?P<strona>[^.]+)', array(
        'methods' => 'GET',
        'callback' => 'getPublicationsEn',
    ));
});




function getNewsEn($data)
{
  $args = array(
    'post_type' => 'news',
    'post_status' => 'publish',
    'posts_per_page' => 4,
    'paged' => $data['strona'],
    'lang' => 'en',
    'orderby' => 'date',
    'order' => 'ASC'
  );

  $loop = new WP_Query($args);
  return render('template-parts/paginacja.php', $loop->posts);
}


function getPublicationsEn($data)
{
  $args = array(
    'post_type' => 'publication',
    'post_status' => 'publish',
    'posts_per_page' => 4,
    'lang' => 'en',
    'paged' => $data['strona'],
    'orderby' => 'date',
    'order' => 'ASC'
  );

  $loop = new WP_Query($args);
  return render('template-parts/paginacja.php', $loop->posts);
}



function getNewsPl($data)
{
    $args = array(
        'post_type' => 'news',
        'post_status' => 'publish',
        'posts_per_page' => 4,
        'paged' => $data['strona'],
        'lang' => 'pl',
        'orderby' => 'date',
        'order' => 'ASC'
    );

    $loop = new WP_Query($args);
    return render('template-parts/paginacja.php', $loop->posts);
}


function getPublicationsPl($data)
{
    $args = array(
        'post_type' => 'publication',
        'post_status' => 'publish',
        'posts_per_page' => 4,
        'lang' => 'pl',
        'paged' => $data['strona'],
        'orderby' => 'date',
        'order' => 'ASC'
    );

    $loop = new WP_Query($args);
    return render('template-parts/paginacja.php', $loop->posts);
}







add_post_type_support('news', 'thumbnail');
add_post_type_support('publication', 'thumbnail');
add_theme_support('post-thumbnails', array('news'));
add_theme_support('post-thumbnails', array('publication'));

register_nav_menus(
  array(
    'primary-menu' => __('Primary Menu'),
    'secondary-menu' => __('Secondary Menu')
  )
);
// LOGIN PAGE

function my_custom_login_logo()
{
  echo '<style type="text/css">
   body {
    background-image: linear-gradient(to top, #537895 0%, #09203f 100%)
   }
   .login h1 a {
    height: 57px!important;
    width: 213px!important;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: 100% auto;
     background-image: url( https://weblider.eu/wp-content/uploads/2019/07/logo@2.png )!important;
     margin-bottom: 0px;
  }
  .login h1:after {
    content: "Strony internetowe, identyfikacja wizualna firmy, kampanie marketingowe Google AdWords.";
    display:block;
    color: #FFF;
    font-size: 13px;
  }
  .login h1 {
    border-bottom: 1px solid #bdbdbd;
    padding-bottom: 20px;
  }
  .login form {
    background-color: rgba( 255,255,255,0.10 );
  }
  .login form .input, .login form input[type=checkbox], .login input[type=text] {
    background-color: rgba( 0,0,0,0 );
    color: #FFF;
    border: 1px solid ( 255,255,255,0.4 );
  }
  .login label{
    color: #FFF;
  }
  .login #backtoblog a, .login #nav a  {
    color: #FFF;
  }
  #login form p {
    text-align: center;
  }
  .login #login_error, .login .message, .login .success {
    color: #FFF;
    background-color: rgba( 255,255,255,0.14 );
  }
  a {
    color: #FFF;
    text-decoration: underline!important;
    }

     </style>';
}

add_action('login_head', 'my_custom_login_logo');

add_filter('login_headerurl', 'custom_loginlogo_url');

function custom_loginlogo_url($url)
{

  return 'http://www.weblider.eu';
}
