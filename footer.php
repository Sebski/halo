<footer class="footer container">
    <p class="my-2 m-lg-0">Copyright &#169 haloStudent</p>
    <div class="halo-logo">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-black-01.svg" alt="logo haloStudent">
    </div>
    <a class="mb-4 m-lg-0" style="color:#403f4c" href="#">Polityka prywatności</a>
</footer>

<script src="/wp-content/themes/weblider/js/jquery.js"></script>
<script src="/wp-content/themes/weblider/js/vendor/flickity.pkgd.min.js"></script>
<script src="/wp-content/themes/weblider/js/vendor/scrollreval.js"></script>
<script src="/wp-content/themes/weblider/js/main.js"></script>
<script src="/wp-content/themes/weblider/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="/wp-content/themes/weblider/js/cookie.js"></script>
<script src="https://unpkg.com/flickity-fullscreen@1/fullscreen.js"></script>


<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB832_bHfRQLmwHuIxiTy8K-5_REW1AAlY&callback=initMap&q=Gliwice" async defer></script>-->


