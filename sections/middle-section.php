<div class="container-hero hero-background d-flex parallax" style="background-position: left; background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 17.png')">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <h2 class="hero-title">Blisko <br><span class="hero-accent-color">spokojnie</span> komfortowo</h2>
        </div>
    </div>
</div>
