<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>haloStudent</title>
    <meta name="description" content="Strona stworzona przez Weblider.eu">
    <meta name="author" content="Weblider.eu">
    <meta name="keywords" content="test, elo, memes">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon-01.png"/>
    <link rel="stylesheet" href="/wp-content/themes/weblider/css/main.css">
    <link rel="stylesheet" href="https://unpkg.com/flickity-fullscreen@1/fullscreen.css">
    <link rel="stylesheet" href="/wp-content/themes/weblider/css/vendor/flickity.css"

</head>
<body>
<header class="container sticky-header ">
    <div class="header-content">
        <div class="halo-logo">
            <a href="/" style="display: flex">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white-01.svg" alt="logo haloStudent">
            </a>
        </div>
        <input type="checkbox" id="nav-toggle" class="nav-toggle">
        <nav id="nav" class="nav">
            <ul>
                <li><a href="/oferta/" class="halo-nav-link ">Oferta</a><p class="halo-submenu">Mieszkania 3 - pokojowa<br>
                    Studia 1/2 osobowe</p></li>
                <li><a href="#" class="halo-nav-link">Cennik</a></li>
                <li><a href="#" class="halo-nav-link">Galeria</a></li>
                <li><a href="#" class="halo-nav-link">Informacje</a></li>
                <li><a href="/kontakt/" class="halo-nav-link">Kontakt</a></li>

            </ul>
        </nav>
        <div class="social-icons">
            <i class="fab fa-facebook-square"></i>
            <i class="fab fa-instagram"></i>
        </div>
        <label for="nav-toggle" class="nav-toggle-label">
            <span></span>
        </label>
    </div>
</header>
</body>

</html>
