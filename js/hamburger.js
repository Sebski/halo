const menuNav = document.querySelector('.HeaderWrapper__Navbar');
const divBurger = document.querySelector('.HeaderWrapper__Hamburger');
const openBurger = document.querySelector('.fa-bars');
const closeBurger = document.querySelector('.fa-times');
const menuElements = document.querySelectorAll('.menu-item');


divBurger.addEventListener('click', () => {
    menuNav.classList.toggle('active');
    openBurger.classList.toggle('show');
    closeBurger.classList.toggle('show');
    document.body.classList.toggle('stop-scroll')
})

menuElements.forEach(item => {
    item.addEventListener('click', () => {
        closeBurger.classList.toggle('show')
        openBurger.classList.toggle('show')
        menuNav.classList.toggle('active')
    })
})