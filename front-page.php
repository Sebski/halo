<?php get_header(); ?>

<?php require 'variables.php'; ?>

<section id="hero">
    <?php require 'sections/hero.php'; ?>
</section>
<section id="about-us" class="container section-mobile anim-bottom">
    <?php require 'sections/about-us.php'; ?>
</section>
<section id="middle">
    <?php require 'sections/middle-section.php'; ?>
</section>
<section id="map" class="container section-mobile anim-bottom">
    <?php require 'sections/map.php'; ?>
</section>
<section id="bottom">
    <?php require 'sections/bottom-section.php'; ?>
</section>

<div id="cookie-bar">
    <div class="cookie-bar-wrap">
        <div>
            Korzystając z naszej strony, zgadzasz się na zapisywanie plików cookies.
        </div>
        <div class="halo-cookie-container">
            <a class="halo-cookie-href" href="#"> Więcej informacji o polityce prywatności </a>
            <div id="accept_cookie" class="btn-cookie">Akceptuję</div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
