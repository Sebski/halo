<?php
/* Template Name: Kontakt */
require 'variables.php';
get_header(); ?>

<div class="container-hero hero-background d-flex" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 4.png')">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <h2 class="hero-title hero-title-primary anim-left"><?php wp_title(''); ?></h2>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
        </div>
    </div>
</div>
<!--first card-->
<div class="container section-mobile anim-bottom">
    <div class="row halo-card">
        <div class="col-lg-12">
            <div class="halo-card-upper">
                <h3 class="halo-card-title">Napisz do nas!</h3>
                <div class="col-lg-12">
                    <div class="contact-wrapper wow fadeInUp" data-wow-delay=".2s">
                        <form id="formularz" data-id="14" class="contact-form">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="single-form">
                                        <input type="text" name="your-name" id="your-name" class="form-input" placeholder="Imię i nazwisko">
                                    </div>
                                </div>
                                <div class="col-md-4 px-lg-1">
                                    <div class="single-form">
                                        <input type="email" name="your-email" id="your-email" class="form-input" placeholder="Adres E-mail">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="single-form">
                                        <input type="text" name="your-subject" id="your-subject" class="form-input" placeholder="Temat">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="single-form">
                                        <textarea name="your-message" id="your-message" class="form-input" rows="4" placeholder="Treść wiadomości"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="halo-contact-bg-btn d-flex">
                                        <button class="halo-contact-btn" id="send-mail" type="button" style="border-color:#ffffff00; ">Wyślij</button>
                                    </div>
                                </div>
                                <div class="contact-alert" id="alert-form"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--middle obrazek-->
<div class="container-hero hero-background">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <iframe
                    width="100%"
                    height="900"
                    style="border:0"
                    loading="lazy"
                    allowfullscreen
                    src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d3035.482281554193!2d18.95491034123313!3d50.226074358846134!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e2!4m5!1s0x4716cea5263bbf97%3A0x765cf7e630a626a1!2s%C5%9Al%C4%85ski%20Uniwersytet%20Medyczny.%20Histopatologiczna%20Pracownia%20Diagnostyczna%20Katedry%20i%20Zak%C5%82adu%20Patomorfologii%2C%20Medyk%C3%B3w%2C%20Katowice!3m2!1d50.225468199999995!2d18.9547192!4m5!1s0x4716ce9f6cee2329%3A0x6223e2c70e1e8a12!2sKarola%20Chodkiewicza%2C%2040-754%20Katowice!3m2!1d50.2278326!2d18.955921699999998!5e0!3m2!1spl!2spl!4v1627633225186!5m2!1spl!2spl"
            <!--                    src="https://www.google.com/maps/d/embed?mid=19kj8iKjWVRmVShWTEUVp2DkDcytyY3NY" >-->
            </iframe>
        </div>
    </div>
</div>
<!--second card-->
<div class="container section-mobile contact-section">
    <div class="row halo-card">
        <div class="halo-card-upper col-lg-8">
            <h3 class="halo-card-title">Dane kontaktowe</h3>
            <p class="halo-card-text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam dolorum maiores
                molestiae possimus quisquam voluptate.
            </p>
            <h4 class="halo-card-subtitle">
                Adres: <br>
                Tel: <br>
                E-mail:
            </h4>
        </div>
    </div>
</div>


<?php get_footer(); ?>
