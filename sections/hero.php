<div class="container-hero hero-background d-flex" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 4.png')">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <h2 class="hero-title hero-title-primary">Twój<br> <span class="hero-accent-color type-anim">idealny </span><br> akademik!</h2>
        </div>
    </div>
</div>
