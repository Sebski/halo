<div class="row halo-card" >
    <div class="halo-card-upper map-card col-lg-8">
        <a href="/kontakt/" class="halo-map" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/map.svg')">
        </a>
    </div>
    <div class="col-lg-4 halo-card-color" style="background-color: #0f2f60">
        <h3 class="halo-card-color-title"><a href="/kontakt/">Kilka <br> kroków <br> od uczelni</a></h3>
    </div>
</div>
