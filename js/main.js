let isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
let logo = document.querySelector('.header-content .halo-logo img');
let flashing_span = document.querySelector('#hero .hero-background .row .hero-title span');
let hero_title = document.querySelector('.hero-background .row .hero-title');

if(isSafari===true) {
    if (window.innerWidth > 992) {
        logo.style.width = "180px";
        logo.style.height = "intrinsic";
        hero_title.style.fontSize = "5rem"
        flashing_span.style.display = "inline-flex";
    } else{
        logo.style.width = "100px";
        logo.style.height = "intrinsic";
        hero_title.style.fontSize = "3.2rem";
        flashing_span.style.display = "inline-flex";
    }
}
function getFormData($form)
{

  var unindexed_array = $form.serializeArray();
  var indexed_array = {};

  $.map(unindexed_array, function (n) {
    indexed_array[n['name']] = n['value'];
  });

  return indexed_array;
}
$('#alert-form').hide();

function submitContact(method, url, fill, formId) {
  var $form = $(formId);

  var serializedData = getFormData($form);
  console.log(serializedData);
  request = $.ajax({
    //uderzam do customowego routingu
    url: url,
    type: method,
    data: serializedData,
    fail: function (data) {
      $(fill).hide().fadeOut(250).html(data.message).fadeIn(350);
    },
    success: function (data) {
      console.log(data);
      $(fill).hide().fadeOut(250).html(data.message).fadeIn(350);
    },
  });
}
$("#send-mail").click(function () {
  let method = "POST";
  let id = $("#formularz").data('id');
  let fill = "#alert-form";
  let url = "/wp-json/contact-form-7/v1/contact-forms/"+id+"/feedback";
  console.log(id);
  submitContact(method, url, fill, '#formularz');
});
$(function() {
  const pgurl = window.location.href;
  const navlinks = $('#nav li .halo-nav-link');

  for (let navlink of navlinks) {
    if(navlink.href === pgurl ){
      navlink.classList.add('active-nav');
    }
    else{
      navlink.classList.remove('active-nav');
    }
  }
});

document.addEventListener('DOMContentLoaded',function(event){
  // array with texts to type in typewriter
  var dataText = [ "idealny", "ulubiony", "najlepszy", "perfekcyjny" ];

  // type one text in the typwriter
  // keeps calling itself until the text is finished
  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < (text.length)) {
      // add next character to h1
      document.querySelector(".type-anim").innerHTML = text.substring(0, i+1) +'<span aria-hidden="true"></span>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback)
      }, 200);
    }
    // text finished, call callback if there is a callback function
    else if (typeof fnCallback == 'function') {
      // call callback after timeout
      setTimeout(fnCallback, 1500);
    }
  }
  // start a typewriter animation for a text in the dataText array
  function StartTextAnimation(i) {
    if (typeof dataText[i] == 'undefined'){
      setTimeout(function() {
        StartTextAnimation(0);
      }, 1000);
    }
    // check if dataText[i] exists
    if (i < dataText[i].length) {
      // text exists! start typewriter animation
      typeWriter(dataText[i], 0, function(){
        // after callback (and whole text has been animated), start next text
        StartTextAnimation(i + 1);
      });
    }
  }
  // start the text animation
  StartTextAnimation(0);
});

ScrollReveal().reveal('.anim-bottom',
    {
    delay: 200,
    duration: 1000,
    distance: '150%',
    origin: 'bottom',
    reset: false
    }
);
ScrollReveal().reveal('.anim-left',
    {
      delay: 100,
      duration: 800,
      distance: '125%',
      origin: 'left',
      reset: false
    }
);
ScrollReveal().reveal('.anim-top',
    {
      delay: 200,
      duration: 1000,
      distance: '150%',
      origin: 'top',
      reset: false,
      mobile: false
    }
);



// $('.pagination-news-link').on('click', function(event) {
//     console.log(this);
//     let language = $('#language').text();
//     console.log(language);
//     $('.pagination-news-link').removeClass('active');
//     $(this).addClass('active')
//     submitData('GET', this.text, '/wp-json/paginacja/v1/'+language+'/getNews/'+this.text, '.news-container');
// })
//
// $('.pagination-publications-link').on('click', function(event) {
//     console.log(this);
//     let language = $('#language').text();
//     console.log(language);
//
//     $('.pagination-publications-link').removeClass('active');
//     $(this).addClass('active')
//     submitData('GET', this.text, '/wp-json/paginacja/v1/'+language+'/getPublications/'+this.text, '.publications-container');
// })
//
// function myFunction() {
//     document.getElementById("myDropdown").classList.toggle("show-languages");
// }

// window.onclick = function(event) {
//     if (!event.target.matches('.dropbtn')) {
//         var dropdowns = document.getElementsByClassName("dropdown-content");
//         var i;
//         for (i = 0; i < dropdowns.length; i++) {
//             var openDropdown = dropdowns[i];
//             if (openDropdown.classList.contains('show-languages')) {
//                 openDropdown.classList.remove('show-languages');
//             }
//         }
//     }
// }

