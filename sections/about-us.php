<div class="row halo-card">
    <div class="col-lg-8 ">
        <div class="halo-card-upper">
            <h3 class="halo-card-title">Poznaj nasz akademik</h3>
            <p class="halo-card-text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam dolorum maiores
                molestiae possimus quisquam voluptate.
                Asperiores cum debitis ea earum eius, enim facilis hic illo, inventore odit perspiciatis quae, quasi.
            </p>
            <h4 class="halo-card-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
        </div>
        <div class="d-flex row halo-card-image-bg">
            <div class="halo-card-image col-md-4 img-fluid" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 4.png')"></div>
            <div class="halo-card-image col-md-4 img-fluid" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 17.png')"></div>
            <div class="halo-card-image col-md-4 img-fluid" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 4.png')"></div>
        </div>
    </div>
    <div class="col-lg-4 halo-card-color" style="background-color: #22adb7;">
        <h3 class="halo-card-color-title"><a href="/oferta/">Zobacz<br>układ<br>pomieszczeń</a></h3>
    </div>
</div>
