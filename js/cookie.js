function readCookie(nameCookie)
{
    let nameEQ = nameCookie + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
            let value = c.substring(nameEQ.length, c.length);
            return value;
        }
    }
    return '';
}

function createCookie(nameCookie, valueCookie, expiresDays=1, path='')
{
    let d = new Date();
    d.setTime(d.getTime() + (expiresDays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    let domain = readCookie('cookie_domain');
    let newCookie = nameCookie + '=' + valueCookie + '; ' + expires + '; path=/' + path + "; domain=" + domain + "; samesite=lax";

    document.cookie = newCookie;
}
function deleteCookie(nameCookie)
{
    createCookie(nameCookie, "", -1);
}
function checkCookie(nameCookie)
{
    let value = readCookie(nameCookie);
    if (value !== '') {
        return true;
    }
    return false;
}
if( checkCookie("visible_cookie") ){
    $("#cookie-bar").hide();
}

$("#accept_cookie").on('click',function(){
    createCookie("visible_cookie", "true", 30);
    $("#cookie-bar").hide();
})
