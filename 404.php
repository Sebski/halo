<?php
/* Template Name: 404 */
require 'variables.php';
get_header(); ?>

<div class="container" style="padding-top:20rem; margin-bottom: 15rem;">
    <div class="row mt-5 pt-5 justify-content-center align-items-center align-content-center">
     Here is nothing. Nothing is everything.
    </div>
</div>

<?php get_footer(); ?>
