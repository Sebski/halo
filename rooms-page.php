<?php
/* Template Name: Oferta */
require 'variables.php';
get_header(); ?>

<div class="container-hero hero-background d-flex" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 4.png')">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <h2 class="hero-title hero-title-primary anim-left"><?php wp_title(''); ?></h2>
        </div>
    </div>
</div>
<!--first card-->
<div class="container section-mobile">
    <div class="row halo-card">
        <div class="col-lg-8">
            <div class="halo-card-upper">
                <h3 class="halo-card-title">Pokój 3-osobowy</h3>
                <div class="main-carousel" data-flickity='{ "cellAlign": "left", "contain": true, "pageDots": true, "imagesLoaded": false, "fullscreen": true, "draggable": false}'>

                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">
                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">
                </div>
                <div class="nav-carousel"  data-flickity='{"asNavFor": ".main-carousel", "contain": true, "pageDots": false, "imagesLoaded": false, "prevNextButtons": false, "draggable": true}'>

                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">
                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                    <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">
                </div>
            </div>
        </div>
        <div class="col-lg-4 halo-subpage-card-color" style="background-color: #22adb7;">
            <div class="halo-subpage-card-color-icons">
                <i class="fas fa-parking halo-subpage-card-color-icon" data-hover="Parking"></i>
                <i class="fas fa-wifi halo-subpage-card-color-icon" data-hover="WiFi"></i>
                <i class="fas fa-warehouse halo-subpage-card-color-icon" data-hover="Garaż"></i>
                <i class="fas fa-key halo-subpage-card-color-icon" data-hover="Klucze hotelowe"></i>
                <i class="fas fa-tv halo-subpage-card-color-icon" data-hover="Telewizja"></i>
            </div>
        </div>
    </div>
</div>

<!--middle obrazek-->
<div class="container-hero hero-background d-flex" style="background-position: left; background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 17.png')">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <h2 class="hero-title">Pokój <span class="hero-accent-color">rekreacyjny</span></h2>
        </div>
    </div>
</div>
<!--second card-->
<div class="container section-mobile">
    <div class="row halo-card">
        <div class="halo-card-upper col-lg-8">
            <h3 class="halo-card-title">Studio 1-osobowe</h3>
            <div class="main-carousel-studio" data-flickity='{ "cellAlign": "left", "contain": true, "pageDots": true, "imagesLoaded": false, "draggable": false, "fullscreen": true}'>

                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">
                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">

            </div>
            <div class="nav-carousel-studio"  data-flickity='{"asNavFor": ".main-carousel-studio", "contain": true, "pageDots": false, "imagesLoaded": false, "prevNextButtons": false, "draggable": true}'>

                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">
                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 4.png' class="carousel-cell halo-flickity-image">
                <img src='<?php echo get_template_directory_uri();?>/assets/img/Rectangle 17.png' class="carousel-cell halo-flickity-image">
            </div>
        </div>
        <div class="col-lg-4 halo-subpage-card-color" style="background-color: #0f2f60">
            <div class="halo-subpage-card-color-icons">
                <i class="fas fa-parking halo-subpage-card-color-icon" data-hover="Parking"></i>
                <i class="fas fa-wifi halo-subpage-card-color-icon" data-hover="WiFi"></i>
                <i class="fas fa-warehouse halo-subpage-card-color-icon" data-hover="Garaż"></i>
                <i class="fas fa-key halo-subpage-card-color-icon" data-hover="Klucze hotelowe"></i>
                <i class="fas fa-tv halo-subpage-card-color-icon" data-hover="Telewizja"></i>
            </div>
        </div>
    </div>
</div>
<!--bottom obrazek-->
<div class="container-hero hero-background d-flex" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/Rectangle 4.png'); background-position: -30em 0em;">
    <div class="row position-relative d-flex">
        <div class="col-lg-12 col-md-12 col-sm-12 hero-title-container">
            <div>
                <h2 class="hero-title mobile-hero-title">Parking|<span class="hero-accent-color">Suszarnia</span></h2>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
